import React, { useCallback, useRef, useState } from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import ImagePreview from './ImagePreview';
import SwipeContainer from './SwipeContainer';
import { ImageObject, IProps, RenderImageProps } from './types';

const { height: deviceHeight, width: deviceWidth } = Dimensions.get('window');

const ImageGallery = (props: IProps) => {
  const { images } = props;

  const thumbSize = 100;
  const resizeMode = 'contain';
  const disableSwipe = false;
  const initialIndex = 0;

  const [activeIndex, setActiveIndex] = useState(0);
  const [isDragging, setIsDragging] = useState(false);
  const topRef = useRef<FlatList>(null);
  const bottomRef = useRef<FlatList>(null);

  const keyExtractorThumb = (item: ImageObject, index: number) =>
    item && item.id ? item.id.toString() : index.toString();
  const keyExtractorImage = (item: ImageObject, index: number) =>
    item && item.id ? item.id.toString() : index.toString();

  const scrollToIndex = (i: number) => {
    setActiveIndex(i);
    if (topRef?.current) {
      topRef.current.scrollToIndex({
        animated: true,
        index: i,
      });
    }
    if (bottomRef?.current) {
      if (i * (thumbSize + 10) - thumbSize / 2 > deviceWidth / 2) {
        bottomRef?.current?.scrollToIndex({
          animated: true,
          index: i,
        });
      } else {
        bottomRef?.current?.scrollToIndex({
          animated: true,
          index: 0,
        });
      }
    }
  };

  const renderItem = ({ item, index }: RenderImageProps) => {
    return <ImagePreview item={item} resizeMode={resizeMode} />;
  };

  const renderThumb = ({ item, index }: RenderImageProps) => {
    return (
      <TouchableOpacity
        onPress={() => scrollToIndex(index)}
        activeOpacity={0.8}
      >
        <Image
          resizeMode={'cover'}
          style={
            activeIndex === index
              ? [styles.thumb, styles.activeThumb, { width: 130, height: 200 }]
              : [styles.thumb, { width: 130, height: 200 }]
          }
          source={{ uri: item.thumbUrl ? item.thumbUrl : item.url }}
        />
      </TouchableOpacity>
    );
  };

  const onMomentumEnd = (e: any) => {
    const { x } = e.nativeEvent.contentOffset;
    scrollToIndex(Math.round(x / deviceWidth));
  };

  const getImageLayout = useCallback((_, index) => {
    return {
      index,
      length: deviceWidth,
      offset: deviceWidth * index,
    };
  }, []);

  const getThumbLayout = useCallback(
    (_, index) => {
      return {
        index,
        length: thumbSize,
        offset: thumbSize * index,
      };
    },
    [thumbSize]
  );

  const ScrollPositionBar = () => {
    return (
      <View
        style={{ backgroundColor: '#F2F2F2', width: 500, height: 20 }}
      ></View>
    );
  };
  return (
    <View style={styles.container}>
      <SwipeContainer disableSwipe={disableSwipe} setIsDragging={setIsDragging}>
        <FlatList
          initialScrollIndex={initialIndex}
          // getItemLayout={getImageLayout}
          data={images}
          horizontal
          keyExtractor={keyExtractorImage}
          onMomentumScrollEnd={onMomentumEnd}
          pagingEnabled
          ref={topRef}
          renderItem={renderItem}
          scrollEnabled={!isDragging}
          showsHorizontalScrollIndicator={false}
        />
      </SwipeContainer>

      <FlatList
        initialScrollIndex={initialIndex}
        getItemLayout={getThumbLayout}
        data={props.images}
        horizontal
        keyExtractor={keyExtractorThumb}
        pagingEnabled
        ref={bottomRef}
        renderItem={renderThumb}
        showsHorizontalScrollIndicator={false}
        style={[styles.bottomFlatlist]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    flex: 1,
    width: deviceWidth,
  },
  activeThumb: {
    opacity: 0.6,
  },
  thumb: {
    borderRadius: 0,
    marginRight: 10,
  },
  bottomFlatlist: {},
});

export default ImageGallery;

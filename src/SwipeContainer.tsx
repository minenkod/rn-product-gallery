import React from 'react';
import PanContainer from './PanContainer';

const SwipeContainer = ({
  children,
  setIsDragging,
  disableSwipe,
}: {
  children: any;
  setIsDragging: any;
  disableSwipe?: boolean;
}) => {
  return disableSwipe ? (
    <>{children}</>
  ) : (
    <PanContainer setIsDragging={setIsDragging}>{children}</PanContainer>
  );
};

export default SwipeContainer;

import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { ImagePreviewProps } from './types';

const { height, width } = Dimensions.get('window');

const ImagePreview = ({ item }: ImagePreviewProps) => {
  return (
    <TouchableWithoutFeedback onPress={() => {}}>
      <View style={{ flex: 1 }}>
        <Image
          resizeMode={'contain'}
          source={{ uri: item.url }}
          style={styles.image}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  image: {
    height: height * 0.5,
    width: width,
  },
});

export default ImagePreview;

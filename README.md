# React Native Nike clone product gallery
<img src="./demo-images/NikeApp.png" width="128"/>
 
# This is a work in progress library, please do not use yet.

## Features

- Image gallery with thumbnails
- Made with `PanResponder`, no external dependencies
- Written in `Typescript`
 
## Installation

#### yarn:

```bash
yarn add rn-product-gallery
```

#### npm:

```bash
npm i rn-product-gallery
```

## Usage

#### Simple Gallery with thumbs:

```tsx
import { ImageGallery } from 'rn-product-gallery';

const images = [
  {
    id: 1,
    url: 'https://...',
    // any other extra info you want
  },
  ...
]

const MyGallery = () => {
    return (
        <View>
            <ImageGallery images={images} />
        </View>
    )
}
```
 
## Props

#### `CachedImage` accepts the following props:

| Properties              | PropType  | Description                                                                                                                                        |
|-------------------------|-----------|                                                                                                       |
| `images`                | `Array`   | (**Required**) array of objects. Every image must have a `url` (required), a thumbUrl (optional) and id (optional) and any other info you need     |
| `resizeMode`            | `string`  | React Native Image component [resizeMode](https://reactnative.dev/docs/image#resizemode) defaults to `contain`, used on images                     |
| `close`                 | `Func`    | (**Required**) function to close the gallery gesture.                                                                                                                        |
| `initialIndex`          | `Number`  | Initial index of image to snap when opening the gallery                                                                                            |
| `thumbSize`             | `Number`  | Height and width of the thumb, defaults to `48`                                                                                                    |
| `thumbResizeMode`       | `string`  | React Native Image component [resizeMode](https://reactnative.dev/docs/image#resizemode) defaults to `cover`, used on thumbnails                   |
| `hideThumbs`            | `Boolean` | Set to true to hide the thumbs defaults to `false`                                                                                                 |

## Todo:

- [ ] Add zoom factor

- [ ] Add prop to keep zoom and double tap to zoom out

## Credit to the following authors:

- [Efstathios Ntonas](https://github.com/efstathiosntonas)
- [George Bakogiannis](https://github.com/geobako)
- [George Kallinikos](https://github.com/giokallis)

export const images = [
  {
    id: 1,
    url: 'https://innerpeace-cdn.s3.eu-west-2.amazonaws.com/9f69c2c0-cf52-4834-b434-bbe42c0b9a32/3b10c179-0d3c-43fc-8405-627f24b0bc7f.jpg',
  },
  {
    id: 2,
    url: 'https://innerpeace-cdn.s3.eu-west-2.amazonaws.com/9f69c2c0-cf52-4834-b434-bbe42c0b9a32/ccaaf8c4-01dd-4327-b06f-170a124a9741.jpg',
    thumbUrl: '',
  },
  {
    id: 3,
    url: 'https://innerpeace-cdn.s3.eu-west-2.amazonaws.com/479f4cb4-cf40-4614-985c-281ef19706a3/468092bc-acd5-4505-b1fc-a733a5fe3804.jpg',
    thumbUrl: '',
  },
  {
    id: 4,
    url: 'https://innerpeace-cdn.s3.eu-west-2.amazonaws.com/479f4cb4-cf40-4614-985c-281ef19706a3/7efb18b4-7530-4637-9cad-3c8a5366d622.jpg',
    thumbUrl: '',
  },
];
